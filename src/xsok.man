.TH XSOK 6 "May 1996" "Handmade"
.SH NAME
xsok \- generic Sokoban game for X11, Version 1.02
.SH SYNOPSIS
.B xsok
[
.I options
]
.SH DESCRIPTION
.B xsok
is a single player strategic game, a superset of the well known Sokoban game.
This manpage describes only the user interface of \fBxsok\fP. If you want to
create own levels, you should consult the \fBxsok\fP manual for more information.

The target of \fBSokoban\fP
is to push all the objects into the score area of each level using the
mouse or the arrow keys. For the other level subsets, there are different
kinds of objects, and special effect squares.

\fBxsok\fP can be played using only the mouse, or only the keyboard.  Keyboard
and mouse bindings are defined through a textfile. This manual page describes
the default bindings.

.SH OPTIONS
All standard X toolkit parameters may be given, such as 
\fB\-display\fP \fIdisplay\fP etc.
Additional options are

.TP 4
.B \-rules \fIlevel subset\fP
This option specifies the initial level subset for \fBxsok\fP.
Valid built-in rule names are \fBSokoban\fP, \fBCyberbox\fP, and \fBXsok\fP,
but you may implement new level subsets without recompiling the game.
Level subsets share common characteristics of the board.
In \fBSokoban\fP, for example, all boxes have the same weight.
In \fBXsok\fP, the first level is a demo level, where you can experiment
with the new objects.
.TP 4
.B \-level \fIstartlevel\fP
Set the starting level.
.TP 4
.B \-username \fIusername\fP
In a save-game file, your name, as found in the \fB/etc/passwd\fP file, and the
hostname of your computer, will be stored in the file. The default format is
\fIrealname (username@hostname.domain)\fP, for example
\fBMichael Bischoff (mbi@flawless.ts.rz.tu-bs.de)\fP.
You can override this default string with the argument to the username option and provide a different e-mail address, for example
.br
\fBxsok -username "Michael Bischoff (m.bischoff@tu-bs.de)"\fP.

If you break the scores for one level, your solution will be saved automatically. 

.TP 4
.B \-xsokdir \fIxsokdir\fP
This option sets the root of the \fBxsok\fP data file tree. The default is
\fB/usr/games/lib/xsok\fP.

.TP 4
.B \-xpmdir \fIxpmdir\fP
This gives the directory from where to load the graphic data.

.TP 4
.B \-savedir \fIsavedir\fP
This option sets the directory for save game files and the \fBxsok\fP highscore
files. The default is \fB/var/games/xsok\fP.

.TP 4
.B \-messageFile \fImessagefile\fP
This option sets the name of an alternative message file for \fBxsok\fP.
The pathname is relative to \fIxsokdir\fP. The default is \fBmessages\fP,
and does not exist, which means to use the internal messages.

.TP 4
.B \-keyboardFile \fIkeyboardfile\fP
This option sets the name of the file defining the keyboard bindings.
The pathname is relative to \fIxsokdir\fP. The default is \fBkeys\fP.
The bindings in the default file are described below.

All command line options may be abbreviated, or set by the X11 resource
manager. The resource name for option \fB\-xyz\fP is \fBTableau.xyz\fP and its
class name \fBTableau.Xyz\fP.

.SH KEYBOARD BINDINGS
The arrow keys will move the man. The default binding is similar to the
binding in \fBxsokoban\fP. Some commands accept a numerical prefix (i.e.
typing some digits before the command key), which usually is used as an
operation count.

.TP 8
.B a
Display the author of a level (if known).
.TP 8
.B b
Drops the bookmark.
.TP 8
.B g
Goto bookmark.
.TP 8
.B i
Displays the level comment (if any).
.TP 8
.B s
Saves the current position.
.TP 8
.B L
Reloads a saved game.
.TP 8
.B R
Restart this level. With numerical prefix \fIn\fP, jumps to move number
\fIn\fP.
.TP 8
.B N
Proceed to the next level. With numerical prefix \fIn\fP,
jumps to level \fIn\fP.
.TP 8
.B H
Reread the highscore table.
.TP 8
.B P
Return to the previous level.
.TP 8
.B U
Proceeds to the next unsolved level.
.TP 8
.B q
Quits the game.
.TP 8
.B v
Shows the version of \fBxsok\fP.
.TP 8
.B ?
Shows the current score.
.TP 8
.B b
Shows the best score for this level.
.TP 8
.B c
Drops the bookmark at the current position.
.TP 8
.B u
Undoes the last elementary move. Accepts numerical prefix.
.TP 8
.B r
Redoes last move (undoes an undo). Accepts numerical prefix.
.TP 8
.B (
Starts recording a macro (sequence of moves)
.TP 8
.B )
End a macro.
.TP 8
.B <ENTER>
Replays a macro.

.SH KEYBOARD BINDINGS
With the default button assignment, button 1 is bound to the
function \fBMouseMove\fP. If pressed on a clear square, the man will move to
that location via the optimal path if such a path exists. If pressed on an
object that is adjacent to the player, the object will be pushed.

Button 2 is bound to \fBMouseDrag\fP. This command requires that you press the
mouse button on a location where a box resides, drag the mouse, and release the
button on an empty square. The man will then move the box from the first square
onto the second with the minimal number of pushes, if it is possible at all.
Please note that the man will not move any other object and will only use
squares without effects.

Button 3 is bound to \fBMouseUndo\fP. This function undoes one of the previous
commands, which would possibly require a lot of calls to the atomic undo function.

.SH NATIONAL LANGUAGE SUPPORT
\fBxsok\fP has simple support for different languages. All messages which
appear in the X11 window may be overloaded by files, as well as the key
bindings.  The typical support consists of an application-defaults file, a
message file, and a keyboard file. Possibly translated online-help files are
also there.  To select a different language, call \fBxsok\fP after setting the
environment variable \fBLANG\fP to the desired value.  Currently, no translated
version is available.

.SH FILES
(Directories may differ on your system.)

 \fB/usr/games/bin/xsok\fP
 \fB/var/games/xsok/\fP\fItype\fP\fB.score\fP
 \fB/var/games/xsok/\fP\fItype\fP\fB.\fP\fInn\fP\fB.{sv,bs,mp,mm}\fP
 \fB/usr/doc/xsok/COPYRIGHT.{GNU,xsok,xpm}\fP
 \fB/usr/doc/xsok/xsok.dvi\fP
 \fB/usr/doc/xsok/cyberbox.doc\fP
 \fB/usr/games/lib/xsok/floor.xpm.gz\fP
 \fB/usr/games/lib/xsok/objects.xpm.gz\fP
 \fB/usr/games/lib/xsok/keys\fP
 \fB/usr/games/lib/xsok/keys.help\fP
 \fB/usr/games/lib/xsok/\fP\fItype\fP\fB.def.gz\fP
 \fB/usr/games/lib/xsok/\fP\fItype\fP\fB.help\fP

Where \fItype\fP is one of \fBSokoban\fP, \fBXsok\fP, \fBCyberbox\fP, and
possibly others.

.SH CREDITS
Inspiration for \fBxsok\fP came from \fBxsokoban\fP, a previous implementation
of the \fBSokoban\fP game by Joseph L. Traub. From this game, the wall graphics
were taken, and the mouse button assignment.  \fBxsokoban\fP's level files can
be used without change, but by default, all level files of a level subset are
combined into a single file.  Of course, credits also go to the unknown author
of the curses based game.

The \fBCyberbox\fP levels (and a MSDOS game of the same name) are written by
Doug Beeferman.

.SH BUGS
The undo function is too slow.  Highscore file handling uses no file locking.

\fBCyberbox\fP zappers are implemented as one-way passages, which causes worse
scores and easier levels.

Please mail bug reports to \fBmbi@mo.math.nat.tu-bs.de\fP.  Fixes are
especially welcome.

.SH SEE ALSO
\fBxsokoban(6x)\fP, \fBsokoban(6)\fP

.SH AUTHOR
Michael Bischoff

.SH COPYRIGHT
Copyright (c) 1994 by Michael Bischoff (\fBmbi@mo.math.nat.tu-bs.de\fP)
.sp 1

Permission to use, copy, modify, and distribute this software and its
documentation for any purpose and without fee is hereby granted, provided that
the above copyright notice appear in all copies and that both that copyright
notice and this permission notice appear in supporting documentation.

\fBxsok\fP was developed under Linux, the free UNIX for the IBM-PC and
compatibles. \fBxsok\fP is distributed by terms of the GNU General public
license (GNU Copyleft).
