/*****************************************************************************/
/*									     */
/*									     */
/*	Xsok version 1.02 -- module mousemove.c				     */
/*									     */
/*	Computes paths to squares in the distance.			     */
/*	Written by Michael Bischoff (mbi@mo.math.nat.tu-bs.de)		     */
/*	November 1994 ... March 1996					     */
/*	see COPYRIGHT.xsok for Copyright details			     */
/*									     */
/*									     */
/*****************************************************************************/
#ifndef _POSIX_SOURCE
#define _POSIX_SOURCE
#endif
#include "xsok.h"
#include <assert.h>

static int before_move;

void cmd_MouseUndo(void) {
    if (lastcmd != cmd_MouseMove &&
	lastcmd != cmd_MousePush &&
	lastcmd != cmd_MouseDrag &&
	lastcmd != cmd_PlayMacro) {
	cmd_UndoMove();	/* normal undo */
	return;
    }
    jumpto_movenr(before_move);
    cmd_ShowScore();
}
    
#define RBSIZE	  1024	/* only powers of 2 will work */
#ifndef sgn
#define sgn(x)		((x) < 0 ? -1 : 1)
#endif

static int noeffect(int eff) {
    switch(eff) {
    case E_NOTHING:
    case E_EXIT:
    case E_DEST:
	return 1;
    default:
	return 0;
    }
}

#define STEP(dx,dy,dir) if ( \
  (map[y+(dy)][x+(dx)]->mask & 1) && \
   noeffect(map[y+(dy)][x+(dx)]->effect) && \
   (map[y+(dy)][x+(dx)]->enter & (1 << dir)) && \
   (map[y+(dy)][x+(dx)]->leave & (1 << dir)) && \
   !obj[y+(dy)][x+(dx)] && \
   dist[y+(dy)][x+(dx)] > d) { \
    dist[remy[wr] = y+(dy)][remx[wr]= x+(dx)] = d; \
    if ((wr = (wr + 1) & (RBSIZE-1)) == rd) break; }

/* search from destination to source */
static int backsteps(unsigned dist[MAXROW][MAXCOL],
		 int x1, int y1, int x2, int y2) {
    int rd = 0, wr = 1, remx[RBSIZE], remy[RBSIZE];
    memset(dist, 0xff, sizeof(unsigned)*MAXROW*MAXCOL);
    if (!(map[y1][x1]->mask & 1))
	return -1;
    remx[0] = x1;
    remy[0] = y1;
    dist[y1][x1] = 0;
    while (rd != wr) {
	int x, y, d;
	if (dist[y2][x2] < 30000)
	    return dist[y2][x2];
	x = remx[rd];
	y = remy[rd];
	rd = (rd+1) & (RBSIZE-1);
	d = 1 + dist[y][x];
	STEP(1,0,1);	/* dir is reverse since we do backward search */
	STEP(0,1,0);
	STEP(-1,0,3);
	STEP(0,-1,2);
    }
    /* no path is found */
    return -1;
}

static void subcmd_pushto(int dx, int dy) {
    int i, dir;

    if (dx) {
	i = abs(dx);
	dir = 2 + sgn(dx);
    } else {
	i = abs(dy);
	dir = 1 + sgn(dy);
    }
    while (i--)
	playermove(dir);
}

void cmd_MousePush(void) {
    int dx, dy;

    before_move = game.n_moves;
    dx = mouse_x - game.x;
    dy = mouse_y - game.y;
    if (dx && dy)
	cmd_MouseMove();
    else
	subcmd_pushto(dx, dy);
}

static void subcmd_moveto(int xx, int yy) {
    unsigned dist[MAXROW][MAXCOL];
    struct objects *ip = obj[game.y][game.x];

    /* case 1: a distance 1 click will move or push */
    if (xx == game.x && yy == game.y)
	return;	/* shortcut! */

    if (abs(xx - game.x) + abs(yy - game.y) == 1) {
	if (xx - game.x)
	    if (xx > game.x)
		playermove(3);
	    else
		playermove(1);
	else
	    if (yy > game.y)
		playermove(2);
	    else
		playermove(0);
	return;
    }
    /* for greater distance, only use free space. */
    
    obj[game.y][game.x] = NULL;
    if (backsteps(dist, xx, yy, game.x, game.y) < 0) {
	obj[game.y][game.x] = ip;
	show_message(TXT_MOVENOTPOSSIBLE);
	return;
    }
    obj[game.y][game.x] = ip;
    while (game.y != yy || game.x != xx) {
	unsigned length;
	int omove;
	length = dist[game.y][game.x] - 1;
	omove = game.n_moves;
	     if (dist[game.y-1][game.x] == length) playermove(0);
	else if (dist[game.y][game.x-1] == length) playermove(1);
	else if (dist[game.y+1][game.x] == length) playermove(2);
	else if (dist[game.y][game.x+1] == length) playermove(3);
	if (dist[game.y][game.x] > length)
	    break;	/* else may cause cycling (with teleporters) */
	if (game.n_moves != omove + 1)
	    break;	/* precomputed move not possible any more */
    }
}

void cmd_MouseMove(void) {
    before_move = game.n_moves;
    subcmd_moveto(mouse_x, mouse_y);
}

static int dxtab[4] = { 0, -1, 0, 1 };
static int dytab[4] = { -1, 0, 1, 0 };

void cmd_MouseDrag(void) {
    struct objects *ip = obj[game.y][game.x], *box;
    unsigned dist[MAXROW][MAXCOL];
    int xx, yy, number[MAXROW][MAXCOL], num = 0;

    before_move = game.n_moves;
    if (mouse_x == mouse_x0 && mouse_y == mouse_y0)
	return;	/* this is a no-op */
    if (!(box = obj[mouse_y0][mouse_x0])) {
	show_message(TXT_NOBOX);
	return;
    }
    obj[game.y][game.x] = NULL;	/* remove player */
    if (obj[mouse_y][mouse_x]) {
	show_message(TXT_ALREADYBOX);
	obj[game.y][game.x] = ip;	/* restore player */
	return;
    }
    if (backsteps(dist, mouse_x0, mouse_y0, game.x, game.y) < 0) {
    notposs:
	obj[game.y][game.x] = ip;
	show_message(TXT_MOVENOTPOSSIBLE);
	return;
    }

    /* number the possibilities for the box */
    num = 1;
    for (xx = 0; xx < game.numcols; ++xx)
	for (yy = 0; yy < game.numrows; ++yy)
	    if ((map[yy][xx]->mask & obj[mouse_y0][mouse_x0]->mask)
		&& !obj[yy][xx])
		number[yy][xx] = num++;
	    else
		number[yy][xx] = -1;
    number[mouse_y0][mouse_x0] = 0;
    if (number[mouse_y][mouse_x] < 0)
	goto notposs;

    {	/* allocate array for the possible box states */
	int d;
	struct boxdist {
	    int pushes;
	    int moves;
	    int x, y;	/* box position */
	    int inqueue;
	    int predecessor;
	} *boxdist;
	int *states, rd = 0, wr = 0;
	boxdist = malloc(4 * num * sizeof(struct boxdist));
	states = malloc(4 * num * sizeof(int));
	for (xx = 0; xx < 4 * num; ++xx) {
	    boxdist[xx].pushes = boxdist[xx].moves = 30000;
	    boxdist[xx].inqueue = 0;
	}
	/* initial positions */
	for (xx = 0; xx < 4; ++xx) {
	    d = backsteps(dist,
			  mouse_x0+dxtab[xx], mouse_y0+dytab[xx],
			  game.x, game.y);
	    if (d >= 0) {
		states[wr++] = xx;
		boxdist[xx].moves = d;
		boxdist[xx].pushes = 0;
		boxdist[xx].x = mouse_x0;
		boxdist[xx].y = mouse_y0;
		boxdist[xx].inqueue = 1;
		boxdist[xx].predecessor = -1;
	    }
	}
	/* loop */
	obj[mouse_y0][mouse_x0] = NULL;	/* remove box */
	while (rd < wr) {
	    /* get position */
	    struct boxdist *now;
	    now = boxdist + (yy = states[rd++]);
	    now->inqueue = 2;
#ifdef TESTING
	    printf("Scanning square %d: %d,%d, pushes=%d, moves=%d\n",
		   yy, now->x, now->y, now->pushes, now->moves);
#endif
	    obj[now->y][now->x] = box;
	    /* player is at x+dxtab[yy & 3], y+dytab[yy & 3] */
	    for (xx = 0; xx < 4; ++xx) {
		int newx, newy, newpos;
		newy = now->y-dytab[xx];
		newx = now->x-dxtab[xx];
		newpos = number[newy][newx];
		if (newpos < 0)
		    continue;
		newpos = 4 * newpos + xx;	/* is index */
		if (boxdist[newpos].pushes <= now->pushes)
		    continue;	/* fewer pushes with the old path */
		/* shift into dir 2^xx */
		if (!(map[newy][newx]->mask & box->mask) ||
		    !(map[now->y][now->x]->mask & 1) ||
		    obj[newy][newx] || obj[now->y+dytab[xx]][now->x+dxtab[xx]])
		    continue;
		d = backsteps(dist,
			      now->x+dxtab[xx], now->y+dytab[xx],
			      now->x+dxtab[yy&3], now->y+dytab[yy&3]);
		if (d < 0)
		    continue;
		d += now->moves;
                if (boxdist[newpos].pushes > now->pushes+1 ||
		    (boxdist[newpos].pushes == now->pushes+1 &&
		    boxdist[newpos].moves > d)) {
		    boxdist[newpos].pushes = now->pushes+1;
		    boxdist[newpos].moves = d;
		    boxdist[newpos].x = newx;
		    boxdist[newpos].y = newy;
		    boxdist[newpos].predecessor = yy;
		    if (!boxdist[newpos].inqueue) {
			boxdist[newpos].inqueue = 1;
			states[wr++] = newpos;
			/* printf("  adding state %d\n", newpos);
		    } else {
			printf("  improving state %d\n", newpos); */
		    }
		    assert(boxdist[newpos].inqueue != 2);
		}
	    }
	    obj[now->y][now->x] = NULL;
	}
	obj[mouse_y0][mouse_x0] = box;	/* restore box */
	free(states);

#ifdef TESTING
	printf("box move from %d,%d to %d,%d requested, %d reachable\n",
	       mouse_x0, mouse_y0, mouse_x, mouse_y, num);
	for (xx = 0; xx < 4; ++xx)
	    printf("xx = %d: pushes = %5d, moves = %5d\n",
		   xx,
		   boxdist[4*number[mouse_y][mouse_x]+xx].pushes,
		   boxdist[4*number[mouse_y][mouse_x]+xx].moves);
#endif
	/* find best end place */
	d = yy = 4*number[mouse_y][mouse_x];
	for (xx = 1; xx < 4; ++xx) {
	    if (boxdist[yy+xx].pushes < boxdist[d].pushes ||
		(boxdist[yy+xx].pushes == boxdist[d].pushes &&
		 boxdist[yy+xx].moves < boxdist[d].moves)) {
		d = yy+xx;
	    }
	}
	if (boxdist[d].pushes == 30000) {
	    free(boxdist);
	    goto notposs;	/* not possible */
	}
	obj[game.y][game.x] = ip;	/* restore player */

	/* no the move definitely is possible */
	/* compute path by predecessor entries */
	xx = -1;	/* last */
	yy = d;		/* current */
	do {
	    boxdist[yy].inqueue = xx;	/* next */
	    xx = yy;
	    yy = boxdist[yy].predecessor;
	} while (yy != -1);
	/* have path. xx is first position to achieve */
	/* first move is different (no push, just setup) */
	xx = boxdist[xx].inqueue;
	while (xx != -1) {
#ifdef TESTING
	    mouse_x = boxdist[xx].x + 2*dxtab[xx&3];
	    mouse_y = boxdist[xx].y + 2*dytab[xx&3];
	    printf("at %d,%d, xx=%d, require moveto %d,%d, pushto %d,%d\n",
		   game.x, game.y, xx, mouse_x, mouse_y,
		   boxdist[xx].x, boxdist[xx].y);
#endif
	    subcmd_moveto(boxdist[xx].x + 2*dxtab[xx&3],
			  boxdist[xx].y + 2*dytab[xx&3]);
#if 0
	    mouse_x = boxdist[xx].x + dxtab[xx&3];
	    mouse_y = boxdist[xx].y + dytab[xx&3];
	    cmd_MousePush();
#else
	    subcmd_pushto(-dxtab[xx&3],-dytab[xx&3]);
#endif
	    xx = boxdist[xx].inqueue;	/* next move */
	}
	free(boxdist);
    }
}
